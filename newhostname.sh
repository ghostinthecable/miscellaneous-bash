#!/bin/bash
#
# Put this scipt in /etc/profile
#
# Make sure this is running as root
if [[ $(whoami) == "root" ]]; then
        echo "let's go" > /dev/null
else
        echo "Run this script as root, pls."
fi
echo ''
# Random hostnames here.
arr[0]="h4cks4lyf3"
arr[1]="unicornfart"
arr[2]="SpicyVanilla"
arr[3]="RedHotMeatballs"
arr[4]="DaisyChainHippie"
arr[5]="iLikeTrains"
# Add more if you like.
#
# Pick a random one;
newhn=$[$RANDOM % ${#arr[@]}]
# Set the hostname;
newhostname="$(echo ${arr[$newhn]})"
hostnamectl set-hostname $newhostname
# Replace in /etc/hosts;
sed -i '/127.0.1.1/d' /etc/hosts
echo "127.0.1.1 $newhostname" >> /etc/hosts
# Exit cleanly;
exit 0
