#!/bin/bash

HEIGHT=15
WIDTH=40
CHOICE_HEIGHT=4
BACKTITLE="Danny's Bar & Restaurant"
TITLE="Race Night Menu"
MENU="Choose one of the following options:"

OPTIONS=(1 "New Bet"
         2 "Remove Bet"
         3 "View Bets")

CHOICE=$(dialog --clear \
                --backtitle "$BACKTITLE" \
                --title "$TITLE" \
                --menu "$MENU" \
                $HEIGHT $WIDTH $CHOICE_HEIGHT \
                "${OPTIONS[@]}" \
                2>&1 >/dev/tty)

clear
case $CHOICE in
        1)
            bash newbet.sh
            ;;
        2)
            echo "Sorry, this option is not available. Please contact admin."

            ;;
        3)
            mysql --user="root" --password="XXX" -e "USE racenight;SELECT raceno FROM races;"

            read -p "Please select race number: " rn

            echo "mysql --user='root' --password='XXX' -e 'USE racenight;SELECT * FROM races WHERE raceno = " >> tempv.sh
            echo $rn >> tempv.sh
            echo "';" >> tempv.sh
            tr -d '\n' < tempv.sh > tempv1.sh
            chmod 777 tempv1.sh
            bash tempv1.sh
            rm tempv.sh tempv1.sh
            ;;
esac
