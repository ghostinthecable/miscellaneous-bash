-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 07, 2019 at 01:21 PM
-- Server version: 5.7.25-0ubuntu0.16.04.2
-- PHP Version: 7.0.32-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `racenight`
--

-- --------------------------------------------------------

--
-- Table structure for table `races`
--

CREATE TABLE `races` (
  `id` int(11) NOT NULL,
  `name` varchar(1024) COLLATE utf8_bin NOT NULL,
  `stake` varchar(1024) COLLATE utf8_bin NOT NULL,
  `raceno` varchar(1024) COLLATE utf8_bin NOT NULL,
  `housewin` varchar(1024) COLLATE utf8_bin DEFAULT NULL,
  `runningwinners` varchar(1024) COLLATE utf8_bin DEFAULT NULL,
  `totalbets` varchar(1024) COLLATE utf8_bin DEFAULT NULL,
  `horseid` varchar(244) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `races`
--

INSERT INTO `races` (`id`, `name`, `stake`, `raceno`, `housewin`, `runningwinners`, `totalbets`, `horseid`) VALUES
(1, 'Daniel', '100', '5', NULL, NULL, NULL, 'LongFace'),
(2, 'Jamie', '225', '10', NULL, NULL, NULL, '4legs'),
(3, 'Sam', '500', '3', NULL, NULL, NULL, 'PurpleThunder');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `races`
--
ALTER TABLE `races`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `races`
--
ALTER TABLE `races`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
