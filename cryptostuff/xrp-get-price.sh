#!/bin/bash

basedir="$(pwd)"

curl -s https://coinmarketcap.com/ > $basedir/cmc
cat $basedir/cmc | grep -o -P 'Ripple Price|.{0,10}\$.{0,10}' > $basedir/cmc1

cat $basedir/cmc1 | grep -E "cmc-link" | head -9 > $basedir/cmcf
cat $basedir/cmcf | tail -1 > $basedir/cmcfl

sed -i 's/cmc-link">//g' $basedir/cmcfl
sed -i 's/<\/a>//g' $basedir/cmcfl
sed -i 's/\$//g' $basedir/cmcfl
sed -i 's/<\///g' $basedir/cmcfl

echo "XRP Current Price: \$$(cat cmcfl)"

sed -i 's/\.//g' $basedir/cmcfl
sed -i 's/\,//g' $basedir/cmcfl
sed -i 's/>//g' $basedir/cmcfl

readcmcfl=$(cat $basedir/cmcfl)
readlastrun=$(cat $basedir/last-xrp-run)

if [ $readcmcfl -gt $readlastrun ]
then
  echo "Price went up"
fi

if [ $readcmcfl -lt $readlastrun ]
then
  echo "Price went down"
fi

if [ $readcmcfl == $readlastrun ]
then
  echo "Price is the same"
fi

cat $basedir/cmcfl > $basedir/last-xrp-run

rm $basedir/cmc*
