#!/bin/bash
#
userpass='userpass.txt'
anitised='sanitised.txt'
passbuilder='passbuilder.txt'
red='\033[0;31m'
green='\033[0;32m'
nocolor='\033[0m'
#
# Get the screenshot
import -window root screenshot.png
convert screenshot.png image.jpg
rm screenshot.png
sha512sum image.jpg > $userpass
cat $userpass | awk '{print $1}' > $anitised
cat $anitised >> $passbuilder
echo ''
output=$(ls /home/)
printf "${green}$output${nocolor}\n\n"
read -p "Please specify the username from the options above: " useracc
echo ''
cat $passbuilder | passwd $useracc
echo ''
finalpass=$(cat $anitised)
printf "${green}New password is:  ${nocolor}" ; printf "${red}$finalpass${nocolor}\n\n"
rm *.txt*
mv image.jpg /var/www/html/protected-dir/image-on-$(date +%F_%R).jpg
