## imgPassAuth  
#### v1.111819 <a href="https://github.com/ghostinthecable/bash/blob/master/imgPassAuth/easypass.sh">Link</a>  

The thesis of imgPassAuth is to generate 100% random sha512 hashes by generating a hash of a unique image.
The unique image itself (for this demonstration) shall be a screenshot.  

To generate a 100% unique image, for this demonstration, we are using a live feed camera of Times Square, New York City (South View) (https://www.earthcam.com/cams/newyork/timessquare/?cam=tstwo_hd).  

The reason for using Times Square, NYC as an example is due to the sheer volume of both pedestrian and motor traffic within the view, these patterns will be very hard to reconstruct, along with the digital signs and possible combinations.  

#### The Screenshot:  
<p align="center">
<img src="http://51.15.25.145/pubimg/timessquare.JPG" title="IMGPA">
</p>  

#### The Process:  
<p align="center">
<img src="http://51.15.25.145/pubimg/imgpa.JPG" title="IMGPA">
</p>  

  
#### Multiview:  
In addition to running with a single view camera, in the following configuration, we have enabled the usage of multiple cameras in different geographical locations, allowing for multi-factor-weather, traffic & lighting components to generate our hashes.  
<p align="center">
<img src="http://51.15.25.145/pubimg/imgpamult.JPG" title="IMGPA">
</p>  

If you really wish to go "Overkill" status, spin up multiple streams in a grid like so:  
<p align="center">
<img src="http://51.15.25.145/pubimg/sudoeasygrid.JPG" title="IMGPA">
</p>  

Once the process has finished, you will find the image located at with a datestamp, like so:  
<p align="center">
<img src="http://51.15.25.145/pubimg/imagesindex.JPG" title="IMGPA">
</p>  

Finally, the image of which we create our password is:
<p align="center">
<img src="http://51.15.25.145/pubimg/gridviewfinal.JPG" title="IMGPA">
</p>  
