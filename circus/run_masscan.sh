#!/bin/bash
read -p "Which ports? (comma separated, no spaces) :  " ports
read -p "Which IP / Range ? ):  " ip
read -p "New log file name:  " log
read -p "Rate? ( (PPS) Packets Per Second):  " rate
masscan -p$ports $ip --banners -oL $log --excludefile='/root/circus/masscan/data/exclude.conf' --rate=$rate
