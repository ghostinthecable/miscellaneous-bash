#!/bin/bash
#begin timer
start=`date +%s`
#vars
alllogs='raw_outputs/*.log*'
everything='raw_outputs/everything.txt'
alive='filtered/still_beating.log'
cams='filtered/cameras.log'
scada='filtered/scada.log'
iis='filtered/iis.log'
nginx='filtered/nginx.log'
apache='filtered/apache.log'
routers='filtered/routers.log'
hydro='filtered/hydro.log'
speakers='filtered/speakers.log'
#organise
cat $alllogs > $everything
cat $everything | grep -E "banner tcp 80" > $alive
#only stuff with banners, search for:
#cameras:
cat $alive | grep -E "Camera" >> $cams
cat $alive | grep -E "camera" >> $cams
cat $alive | grep -E "CAMERA" >> $cams
cat $alive | grep -E "CAM" >> $cams
cat $alive | grep -E "IPcam" >> $cams
cat $alive | grep -E "IPCam" >> $cams
cat $alive | grep -E "IPCAM" >> $cams
cat $alive | grep -E "ipcam" >> $cams
cat $alive | grep -E "Ipcam" >> $cams
cat $alive | grep -E "iPcam" >> $cams
cat $alive | grep -E "iPCam" >> $cams
cat $alive | grep -E "surveillance" >> $cams
cat $alive | grep -E "Surveillance" >> $cams
#miscellaneous:
cat $alive | grep -E "scada" >> $scada
cat $alive | grep -E "Microsoft-IIS" >> $iis
cat $alive | grep -E "nginx" >> $nginx
cat $alive | grep -E "apache" >> $apache
cat $alive | grep -E "router" >> $routers
cat $alive | grep -E "Router" >> $routers
cat $alive | grep -E "Hydro" >> $hydro
cat $alive | grep -E "Hydra" >> $hydro
cat $alive | grep -E "Sonos" >> $speakers
cat $alive | grep -E "Audio" >> $speakers

#cat $alive | grep -E "" >> $

end=`date +%s`

runtime=$((end-start))

echo "Standard filters applied in: " $runtime " sec."

while true; do
    read -p "Search for something in specific? " yn
    case $yn in

        [Yy]* ) read -p "What would you like to search for? " cs
                cat $alive | grep -E $cs >> filtered/$cs.log
                break;;

        [Nn]* ) exit;;

        * ) echo "Please answer yes or no.";;
    esac
done
