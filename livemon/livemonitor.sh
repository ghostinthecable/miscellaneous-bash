#!/bin/bash
#le variables
#start=`date +%s`
basedir='/root/scripts/livemon'
logfile='/var/log/auth.log'
weblogfile='/var/log/apache2/access.log'
readfileone='/root/scripts/livemon/stepone.nlf'
readfiletwo='/root/scripts/livemon/steptwo.nlf'
nlf='/root/scripts/livemon/stepthree.nlf'
#MySQL Creds (ensure to setup https://github.com/ghostinthecable/bash/blob/master/livemon/database.sql first)
user="username"
pass="password"

#pseudocode:
#tail -500 /path/to/log | grep 'yourqueryhere' > /path/to/output

#Run in a loop:
#for i in 1 2 3 4 5 7 8 9
#do
#   echo ''
#   echo "Beginning Snapshot #$i / 9..."
#   echo ''

#Get all SSH logs
cat $logfile | grep 'sshd' > $readfileone

#Gather the attacker IPs in case you need to forward them on (uncomment deletion below)
cat $readfileone | grep -E -o '(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)' | awk '{print $0}' >> $readfiletwo

#Create a unique list of addresses
sort $readfiletwo | uniq > $nlf

#Keep copy of IP addresses with datestamp:
cat $nlf > $basedir/unique-attacker-IPaddr-generated-on-$(date "+%Y.%m.%d-%H.%M.%S").log

#read -r -p "Would you like to delete the debugging files? [y/N] " response
#case "$response" in
#    [yY][eE][sS]|[yY])

#no real need for these anymore, unless debugging
rm $readfileone $readfiletwo $nlf

#Unique IPs only:
sort /root/scripts/livemon/*.log* | uniq > /root/scripts/livemon/uniqips.txt

#clear previous(ssh)
mysql --user="$user" --password="$pass" -e "USE livemon;truncate table iplist;" 2>/dev/null
mysql --user="$user" --password="$pass" -e "USE livemon;ALTER TABLE iplist AUTO_INCREMENT = 1;" 2>/dev/null

#load new dataset(ssh)
mysql --user="$user" --password="$pass" -e "USE livemon;LOAD DATA LOCAL INFILE '/root/scripts/livemon/uniqips.txt' INTO TABLE iplist FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' (ipaddr);" 2>/dev/null
mysql --user="$user" --password="$pass" -e "USE livemon;select ipaddr INTO OUTFILE '/var/lib/mysql-files/final.txt' FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' FROM iplist;" 2>/dev/null
cp /var/lib/mysql-files/final.txt /root/scripts/livemon/

echo ''

echo 'SSH Attackers Count: '
mysql --user="$user" --password="$pass" -e "USE livemon;select count(*) from iplist;" 2>/dev/null
echo ''
echo ''
echo '******'
echo ''

sleep 10
rm final.txt

#Uncomment for loop:
#done
#bash /root/scripts/livemon/livemonitor.sh

/root/scripts/livemon/viewer.sh

exit
