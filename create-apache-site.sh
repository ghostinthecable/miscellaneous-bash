#!/bin/bash

echo ''
read -p "Please introduce the new client's name: " clname
while true; do
    read -p "Is '$clname' correct? (Y/n): " yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) read -p "Please introduce the new client's name: " clname ; break;;
        * ) echo "Please answer yes or no.";;
    esac
done
echo ''
read -p "Please specify a name for the new web directory to be created: " webdir
while true; do
    read -p "Is '/var/www/html/$webdir' correct? (Y/n): " yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) read -p "Please specify a name for the new web directory to be created: " webdir ; break;;
        * ) echo "Please answer yes or no.";;
    esac
done
echo ''
read -p "Please specify a new domain for this site (with leading 'www.'): " domain
while true; do
    read -p "Is '$domain' correct? (Y/n): " yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) read -p "Please specify a new domain for this site: " domain ; break;;
        * ) echo "Please answer yes or no.";;
    esac
done
echo ''
read -p "Please introduce an admin email addr.: " admemail
while true; do
    read -p "Is $admemail correct? (Y/n): " yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) read -p "Please introduce an admin email addr.: " admemail ; break;;
        * ) echo "Please answer yes or no.";;
    esac
done
echo ''
echo "Configuring web directory for $clname with admin email: $admemail..."
DIR="/var/www/html/$webdir"
if [ -d "$DIR" ]; then
  echo "${DIR} already exists."
while true; do
    read -p "Do you wish to continue anyway? (Y/n): " yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) echo "Exiting." ; exit;;
        * ) echo "Please answer yes or no.";;
    esac
done
else
mkdir /var/www/html/$webdir
chown www-data:www-data /var/www/html/$webdir
if [ -d "$DIR" ]; then
  echo "${DIR} has been created successfully."
fi
fi
echo ''
echo "Creating clone of /etc/apache2/sites-available/000-default.conf..."
fulldomain="$domain"
domain=${domain:4}
cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/$domain.conf
echo ''
FILE=/etc/apache2/sites-available/$domain.conf
if [ -f "$FILE" ]; then
    echo "Your configuration file (/etc/apache2/sites-available/$domain.conf) has been created successfully!"
  else
    echo "Uh Oh! Something Goof'd while making your config file in '/etc/apache2/sites-available/'!" ; exit 1
fi
echo ''
echo "Proceeding with setting up your virtual host..."
echo ''
sed -i "s/#ServerName www.example.com/ServerName $fulldomain/g" /etc/apache2/sites-available/$domain.conf
sed -i "/ServerName $fulldomain/a ServerAlias $domain" /etc/apache2/sites-available/$domain.conf
sed -i 's/ServerAlias/	ServerAlias/g' /etc/apache2/sites-available/$domain.conf
sed -i "s/ServerAdmin webmaster\@localhost/ServerAdmin $admemail/g" /etc/apache2/sites-available/$domain.conf
sed -i 's/ServerAdmin/  ServerAdmin/g' /etc/apache2/sites-available/$domain.conf
sed -i "s/DocumentRoot \/var\/www\/html/DocumentRoot \/var\/www\/html\/$webdir/g" /etc/apache2/sites-available/$domain.conf
sed -i 's/DocumentRoot/  DocumentRoot/g' /etc/apache2/sites-available/$domain.conf
#echo "Your virtualhost config file entries: "
#cat /etc/apache2/sites-available/$domain.conf | grep -E "ServerName"
#cat /etc/apache2/sites-available/$domain.conf | grep -E "ServerAlias"
#cat /etc/apache2/sites-available/$domain.conf | grep -E "ServerAdmin"
#cat /etc/apache2/sites-available/$domain.conf | grep -E "DocumentRoot"
#echo ''
while true; do
    read -p "Do you wish to activate this virtualhost now? (Y/n): " yn
    case $yn in
        [Yy]* ) sudo a2ensite $domain ; service apache2 reload ; break;;
        [Nn]* ) break;;
        * ) echo "Please answer yes or no.";;
    esac
done
echo ''
while true; do
    read -p "Do you wish to apply SSL with Certbot? (Y/n): " yn
    case $yn in
        [Yy]* ) git clone https://github.com/certbot/certbot/ ; cd $(pwd)/certbot/ ; ./certbot-auto -d $fulldomain -d $domain ; break;;
        [Nn]* ) break;;
        * ) echo "Please answer yes or no.";;
    esac
done
echo ''
echo "Done."
exit
