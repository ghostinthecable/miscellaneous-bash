#!/bin/bash

echo "Option 1: "
< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c${1:-32}; echo;

echo "Option 2: "
tr -cd '[:alnum:]' < /dev/urandom | fold -w30 | head -n1

echo "Option 3: "
date +%s | sha256sum | base64 | head -c 32 ; echo
