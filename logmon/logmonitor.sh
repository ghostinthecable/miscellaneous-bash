#!/bin/bash

#variables
start=`date +%s`
basedir='/root/admin/scripts/logmon'
logfile='/var/log/auth.log'
sshres='/root/admin/scripts/logmon/attackers-list.txt'
invalid='/root/admin/scripts/logmon/invalid-users.txt'
rootfail='/root/admin/scripts/logmon/3-root-fails.txt'
fullip='/root/admin/scripts/logmon/iplist.txt'
uniqip='/root/admin/scripts/logmon/unique-iplist.txt'

#search logs

#pseudocode:
#tail -500 /path/to/log | grep 'yourqueryhere' > /path/to/output

#predetermined checks
cat $logfile | grep 'sshd' > $sshres
cat $sshres | grep -E 'Failed password for invalid user' >> $invalid
cat $sshres | grep -E 'error: maximum authentication attempts exceeded for root' >> $rootfail

#gather the attacker ips in case you need to forward them on
cat $sshres | grep -E -o '(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)' | awk '{print $0}' >> $fullip

#create a unique list of addresses
sort $fullip | uniq > $uniqip

#timestamp the attackers list
cat $uniqip > $basedir/attackers-caught-on-$(date "+%Y.%m.%d-%H.%M.%S").txt

#read -r -p "Would you like to delete the debugging files? [y/N] " response
#case "$response" in
#    [yY][eE][sS]|[yY]) 

#no real need for these anymore, unless debugging
rm $sshres $invalid $rootfail $fullip $uniqip

#        ;;
#esac

end=`date +%s`

runtime=$((end-start))

echo "Successfully completed in: " $runtime"s"

exit
