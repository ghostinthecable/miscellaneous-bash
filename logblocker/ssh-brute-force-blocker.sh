#!/bin/bash
#
# Created by Daniel Ward for anyone with a pulse;
#
echo "Loading variables..."
# Load Variables;
# Files
logsrc='/var/log/auth.log'
procfile='/root/scripts/logblocker/temp/procfile.txt'
procfile1='/root/scripts/logblocker/temp/procfile1.txt'
procfile2='/root/scripts/logblocker/temp/procfile2.txt'
procfile3='/root/scripts/logblocker/temp/procfile3.txt'
procfile4='/root/scripts/logblocker/temp/procfile4.txt'
procfile5='/root/scripts/logblocker/temp/procfile5.txt'
# Colours
lightgreen='\033[1;32m'
lightpurple='\033[1;35m'
red='\033[0;31m'
nc='\033[0m'
#
# Do the thing, Leroy;
echo "Gathering true attackers (wrong users and root attempts) from $logsrc into $procfile..."
cat $logsrc | grep -E "Invalid user" >> $procfile
cat $logsrc | grep -E "maximum authentication attempts exceeded for root" >> $procfile
echo "Getting IPs into our processing file..."
# Get count of IP addresses in file
cat $procfile | grep -E -o '(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)' | awk '{print $0}' >> $procfile1
if [ ! -f "$procfile1" ]
then
        printf "+  ${red} ERROR! Something failed!${nc}\n\n"
        echo "exiting"
        exit
fi
        printf "+  IP file created: ${lightgreen}OK${nc}\n\n"
#
echo "Counting your attackers per IP..."
uniq -c $procfile1 | awk '{print $2": "$1}' >> $procfile2
if [ ! -f "$procfile" ]
then
        printf "+  ${red} ERROR! Something failed!${nc}\n\n"
        echo "exiting"
        exit
fi
        printf "+  Unique Attacker file created: "
        if [ `wc -l $procfile1 | awk '{print $1}'` -ge "2" ];then
        printf "${red}$(cat $procfile1 | wc -l) Unique Attackers Found${nc}\n\n"
        fi
#
echo "Getting attackers who have tried >= 5 times..."
# Create a threshold of 5 attempts per attacker (IP addr)
awk ' $2 >= 5 ' $procfile2 > $procfile3
if [ ! -f "$procfile2" ]
then
        printf "+  ${red} ERROR! Something failed!${nc}\n\n"
        echo "exiting"
        exit
fi
        printf "+  Serial Attacker file created: "
        if [ `wc -l $procfile2 | awk '{print $1}'` -ge "2" ];then
        printf "${red}$(cat $procfile2 | wc -l) Unique Serial Attackers Found${nc}\n\n"
        fi
#
echo "Sharpening the blade..."
cat $procfile3 | awk '{print "ufw deny from",$1,"to any"}' > $procfile4
#
sed -i 's/: to/ to/g' $procfile4
#
echo "Displaying top 10 lines of blockfile..."
tail -10 $procfile4
#
while true; do
    read -p "Do you wish to block all $(cat $procfile4 | wc -l) addresses? (y/n)  " yn
    case $yn in
        [Yy]* ) bash $procfile4; break;;
        [Nn]* ) echo "Removing source files..."; rm -rf $procfile $procfile1 $procfile2 $procfile3 $procfile4; exit;;
        * ) echo "Please answer yes or no, muggle.";;
    esac
done
#
# Delete temp files
echo "Removing source files..."
rm -rf $procfile $procfile1 $procfile2 $procfile3 $procfile4
#
# Adios
exit
