#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
#INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
#IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
#WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
#OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
#USAGE:
#1) bash compare.sh
#2) Name of archive 1
#3) Name of archive 2
#4) Program will run and spit out results in: results-(date).csv
#
#START:
echo "Full filepaths are required: "
read -p "1st archive: " first
read -p "2nd archive: " second
comm -12 <(sort $first) <(sort $second) > results-$(date "+%Y.%m.%d-%H.%M.%S").csv
while true; do
    read -p "Would you like to run again? (y/n)" yn
    case $yn in
        [Yy]* ) bash compare.sh; exit;;
        [Nn]* ) exit;;
        * ) echo "Please answer (y)es or (n)o.";;
    esac
done
exit;
